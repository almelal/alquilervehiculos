package Negocio;


import java.util.HashMap;

public class Sucursal {
private int id;	
private String direccion;
private HashMap<Integer, Reserva> listaRecogida = new HashMap<Integer, Reserva>();
private HashMap<Integer, Reserva> listaDevolucion = new HashMap<Integer, Reserva>();
private HashMap<String, Empleado> listaEmpleado = new HashMap<String, Empleado>();
private HashMap<String, Coche> listaCoche = new HashMap<String, Coche>();


public Sucursal(int id, String direccion) {
	super();
	this.id = id;
	this.direccion = direccion;
}


public int getId() {
	return id;
}


public void setId(int id) {
	this.id = id;
}


public String getDireccion() {
	return direccion;
}


public void setDireccion(String direccion) {
	this.direccion = direccion;
}


public HashMap<Integer, Reserva> getListaRecogida() {
	return listaRecogida;
}


public void setListaRecogida(HashMap<Integer, Reserva> listaRecogida) {
	this.listaRecogida = listaRecogida;
}


public HashMap<Integer, Reserva> getListaDevolucion() {
	return listaDevolucion;
}


public void setListaDevolucion(HashMap<Integer, Reserva> listaDevolucion) {
	this.listaDevolucion = listaDevolucion;
}


public HashMap<String, Empleado> getListaEmpleado() {
	return listaEmpleado;
}


public void setListaEmpleado(HashMap<String, Empleado> listaEmpleado) {
	this.listaEmpleado = listaEmpleado;
}


public HashMap<String, Coche> getListaCoche() {
	return listaCoche;
}


public void setListaCoche(HashMap<String, Coche> listaCoche) {
	this.listaCoche = listaCoche;
}


@Override
public String toString() {
	return "Sucursal [id=" + id + ", direccion=" + direccion + "]";
}

}
