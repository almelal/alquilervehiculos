package Negocio;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import excepciones.DAOExcepcion;
import persistencia.DAL;
import persistencia.dto.CategoriaDTO;
import persistencia.dto.ClienteDTO;
import persistencia.dto.CocheDTO;
import persistencia.dto.EmpleadoDTO;
import persistencia.dto.EntregaDTO;
import persistencia.dto.ReservaDTO;
import persistencia.dto.SucursalDTO;

public class AlquilerVehiculos{
	
	private HashMap<String, Cliente> listaCliente = new HashMap<String, Cliente>();
	private HashMap<Integer, Sucursal> listaSucursal = new HashMap<Integer, Sucursal>();
	private HashMap<Integer, Reserva> listaReserva = new HashMap<Integer, Reserva>();
	private HashMap<String, Categoria> listaCategoria = new HashMap<String, Categoria>();
	
	private DAL dal;
	
	public AlquilerVehiculos() throws DAOExcepcion{
		this.dal = DAL.dameDAL();
		cargaSistema();
	}
	private static AlquilerVehiculos instancia= null;
	public static AlquilerVehiculos newInstancia() throws DAOExcepcion{
		
		if(instancia == null) instancia = new AlquilerVehiculos();
		return instancia;
	}
	
	
	 
	public HashMap<String, Cliente> getListaCliente() {
		return listaCliente;
	}



	public void setListaCliente(HashMap<String, Cliente> listaCliente) {
		this.listaCliente = listaCliente;
	}



	public HashMap<Integer, Sucursal> getListaSucursal() {
		return listaSucursal;
	}



	public void setListaSucursal(HashMap<Integer, Sucursal> listaSucursal) {
		this.listaSucursal = listaSucursal;
	}



	public HashMap<Integer, Reserva> getListaReserva() {
		return listaReserva;
	}



	public void setListaReserva(HashMap<Integer, Reserva> listaReserva) {
		this.listaReserva = listaReserva;
	}



	public HashMap<String, Categoria> getListaCategoria() {
		return listaCategoria;
	}



	public void setListaCategoria(HashMap<String, Categoria> listaCategoria) {
		this.listaCategoria = listaCategoria;
	}
	public int encontrarS(String nom){
		for(Sucursal su:listaSucursal.values()){
			if(su.getDireccion().equals(nom)){
				return su.getId();
			}
		}
		return -1;
	}
	public void crearEntrega(Entrega entrega){
		dal.crearEntrega(new EntregaDTO(
				entrega.getId()
				, entrega.getFecha()
				, entrega.getTipoSeguro()
				, entrega.getKms()
				, entrega.getCombustible()
				, entrega.getCoche().getMatricula()
				, entrega.getEmpleado().getDni()));
		añadirEntrega(entrega);
		}
	
	public int crearReserva(LocalDateTime fechaRecogida, LocalDateTime fechaDevolucion,
							 int idR, int idD,String dniCliente, String idCat, int modalidadAlquiler){
		int i;
		do{		i = (int) (Math.random()*10000);		}while(listaReserva.containsKey(i));
		
		Reserva r = new Reserva(i, fechaRecogida, fechaDevolucion, modalidadAlquiler,listaCliente.get(dniCliente), listaCategoria.get(idCat),listaSucursal.get(idR),listaSucursal.get(idD));
		 crearReserva(r);
		 return i;
	}
	public void crearReserva(Reserva re){
		añadirReserva(re);
		dal.crearReserva(new ReservaDTO(re.getIdentificador()
				,re.getFechaRecogida()
				,re.getFechaDevolucion()
				,re.getModalidadAlquiler()
				,re.getCliente().getIdentificador()
				,re.getCategoriaAsociada().getNombre()
				,re.getLugarD().getId()
				,re.getLugarR().getId()));
	}
	
	
	
	public void crearCliente(String id, String nombreyApellidos, String direccion, String poblacion,String codPostal, LocalDateTime fechaCarnetConducir, 
							String digitosTC, int mesTC, int añoTC, int cvcTC, String tipoTC){
		Cliente cl = new Cliente(id,nombreyApellidos, direccion, poblacion, codPostal, fechaCarnetConducir, digitosTC, mesTC, añoTC, cvcTC, tipoTC);
		crearCliente(cl);
	}
	public void crearCliente(Cliente cl){
		añadirCliente(cl);
		dal.crearCliente(new ClienteDTO(cl.getIdentificador()
									   ,cl.getNombreyApellidos()
									   ,cl.getDireccion()
									   ,cl.getPoblacion()
									   ,cl.getCodPostal()
									   ,cl.getFechaCarnetConducir()
									   ,cl.getDigitosTC()
									   ,cl.getMesTC()
									   ,cl.getAñoTC()
									   ,cl.getCvcTC()
									   ,cl.getTipoTC()));
		
	}
	public ArrayList<Reserva> listarReservaSucursal(int id){
		ArrayList<Reserva> r1 = new ArrayList<Reserva>(listaSucursal.get(id).getListaDevolucion().values());
		r1.addAll(listaSucursal.get(id).getListaRecogida().values());
		return r1;
	}
	private void añadirCategoria(Categoria cat1){
		listaCategoria.put(cat1.getNombre(),cat1);
	}
	
	private Categoria buscarCategoria(String nombre){
		return listaCategoria.get(nombre);
	}
	private void añadirCliente(Cliente cli1){
		listaCliente.put(cli1.getIdentificador(),cli1);
	}
	
	private void añadirSucursal(Sucursal su){
		listaSucursal.put(su.getId(),su);
	}
	private void añadirReserva(Reserva re){
		listaReserva.put(re.getIdentificador(),re);
		listaSucursal.get(re.getLugarD().getId()).getListaRecogida().put(re.getIdentificador(), re);
		listaSucursal.get(re.getLugarR().getId()).getListaDevolucion().put(re.getIdentificador(), re);
		re.getCliente().getListaReserva().put(re.getIdentificador() ,re);
	}
	//Añadimos el coche a su respectiva
	private void añadirCoche(Coche co){
		if(co.getSucursal() != null) co.getSucursal().getListaCoche().put(co.getMatricula(), co);
		co.getCategoria().getListaCoches().put(co.getMatricula(), co);
	
		}
	private void añadirEmpleado(Empleado e){
		listaSucursal.get(e.getSucursal().getId()).getListaEmpleado().put(e.getDni(), e);
		
	}
	private void añadirEntrega(Entrega en){
		try{
			en.getCoche().getListaEntrega().put(en.getId(),en);
			en.getEmpleado().getListaEntrega().put(en.getId(),en);
			listaReserva.get(en.getId()).setEntrega(en);
			}catch(Exception e){e.printStackTrace();}
	}
	/*****************************/
	/***** CARGA DEL SISTEMA******/
	/*****************************/
	private void cargaSistema(){	
	cargaCategorias();
	cargaSucursales();
	cargaClientes();
	cargaReservas();
	cargarCoches();
	cargarEmpleados();
	cargarEntregas();
	
	}
	
	private void cargaCategorias(){
		List<CategoriaDTO> listacatDTO = dal.obtenerCategorias();
		//crear y añadir todas las categorias a la coleccion
		for (CategoriaDTO catDTO : listacatDTO) {
			añadirCategoria(new Categoria(catDTO.getNombre(),
			catDTO.getPrecioModIlimitada(), catDTO.getPrecioModKms(),
			catDTO.getPrecioKMModKms(), catDTO.getPrecioSeguroTRiesgo(),
			catDTO.getPrecioSeguroTerceros()));
			 }
		// Actualizar los enlaces que representan la relación “superior”
		for (CategoriaDTO catDTO : listacatDTO)
		if (catDTO.getNombreCategoriaSuperior() != null)
			buscarCategoria(catDTO.getNombre()).setSuperior(buscarCategoria(catDTO.getNombreCategoriaSuperior()));
	}

	private void cargaSucursales(){
		List<SucursalDTO> listasuDTO = dal.obtenerSucursales();
		for (SucursalDTO suDTO : listasuDTO) {
			añadirSucursal(new Sucursal(
					suDTO.getId(),
					suDTO.getDirección()
					));
			 }
	}
	private void cargaReservas(){
		List<ReservaDTO> listareDTO = dal.obtenerReservas();
		for(ReservaDTO reDTO : listareDTO){
			añadirReserva(new Reserva(reDTO.getId()
					,reDTO.getFechaRecogida()
					,reDTO.getFechaDevolucion()
					,reDTO.getModalidadAlquiler()
					,listaCliente.get(reDTO.getDniCliente())
					,listaCategoria.get(reDTO.getNombreCategoria())
					,listaSucursal.get(reDTO.getIdSucursalRecogida())
					,listaSucursal.get(reDTO.getIdSucursalDevolucion())));
		}
	}
	
	private void cargaClientes(){
	List<ClienteDTO> listaclDTO = dal.obtenerClientes();
	for (ClienteDTO clDTO : listaclDTO) {
		añadirCliente(new Cliente(
				clDTO.getDni(),
				clDTO.getNombreyApellidos(),
				clDTO.getDireccion(),
				clDTO.getPoblacion(),
				clDTO.getCodPostal(),
				clDTO.getFechaCanetConducir(),
				clDTO.getDigitosTC(),
				clDTO.getMesTC(),
				clDTO.getAñoTC(),
				clDTO.getCvcTC(),
				clDTO.getTipoTC()));
		 }
}
	
	private void cargarCoches(){
		List<CocheDTO> listaCochesDTO = dal.obtenerCoches();		
		for(CocheDTO coDTO:listaCochesDTO){
			añadirCoche(new Coche(coDTO.getMatricula()
					,coDTO.getNombreGama()
					,coDTO.getKm()
					,listaSucursal.get(coDTO.getIdSucursal())
					,listaCategoria.get(coDTO.getNombreCategoria())));
		}
	}
	private void cargarEmpleados(){
		List<EmpleadoDTO> listaEmpleadosDTO = dal.obtenerEmpleados();		
		for(EmpleadoDTO emDTO:listaEmpleadosDTO){
			añadirEmpleado(new Empleado(
					emDTO.getDni()
					, emDTO.getNombre()
					, listaSucursal.get(emDTO.getSucursal())));
		}
	}
	
	private void cargarEntregas(){
		List<EntregaDTO> listaEntregasDTO = dal.obtenerEntregas();		
		
		for(EntregaDTO enDTO:listaEntregasDTO){
			Categoria cat = listaReserva.get(enDTO.getId()).getCategoriaAsociada();
			Coche co = cat.getListaCoches().get(enDTO.getCocheAsignado());
			
			while (co==null){
				cat = cat.getSuperior();
				co = cat.getListaCoches().get(enDTO.getCocheAsignado());//null pointer porque hay coches que pueden estar en una reserva cuya categoria sea menor a la del mismo
			}
			
			
			Empleado em = listaReserva.get(enDTO.getId()).getLugarR().getListaEmpleado().get(enDTO.getDniEmp());
			Entrega en = new Entrega(
					  enDTO.getId()
					, enDTO.getFecha()
					, enDTO.getTipoSeguro()
					, enDTO.getKms()
					, enDTO.getCombustible()
					, co
					, em);
			añadirEntrega(en);
		}
	}
	public List<ReservaDTO> getReservasPorSucursal(int i){
		List<ReservaDTO> listaDTO = dal.obtenerReservasPorSucursalOrigen(i);
		return listaDTO;	 
	}
	
	public List<CocheDTO> getCochesSucursal (int idSucursal) throws SQLException{
		List<CocheDTO> listaCochesDTO = dal.obtenerCochesPorSucursal(idSucursal);		
		return listaCochesDTO;
	}
	
	public List<Empleado> getEmpleados(){
		List<EmpleadoDTO> empleadosDTO = dal.obtenerEmpleados();
		List<Empleado> empleados = new ArrayList<Empleado>();
		for(EmpleadoDTO em : empleadosDTO){
		empleados.add(new Empleado(em.getDni()
				, em.getNombre()
				,listaSucursal.get(em.getSucursal())));
		}
		return empleados;		
	}
	
	public Empleado getEmpleado(String dni){
		EmpleadoDTO empleadoDTO = dal.obtenerEmpleado(dni);
		return new Empleado(empleadoDTO.getDni()
				, empleadoDTO.getNombre()
				,listaSucursal.get(empleadoDTO.getSucursal()));
	}
	public List<EntregaDTO> getEntregas(){
		return dal.obtenerEntregas();
	}
}
