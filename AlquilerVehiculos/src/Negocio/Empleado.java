package Negocio;

import java.util.HashMap;

public class Empleado {
	private String dni;
	private String nombre;
	private String administrador;
	private Sucursal sucursal;
	public HashMap<Integer, Entrega> listaEntrega = new HashMap<Integer, Entrega>();
	public HashMap<String, Devolucion> listaDevolucion = new HashMap<String, Devolucion>();
	public Empleado(String dni,String nombre, Sucursal sucursal) {
		this.dni = dni;
		this.nombre = nombre;
		this.sucursal = sucursal;
	}
	public Empleado(String dni,String nombre, String administrador) {
		this.dni = dni;
		this.nombre = nombre;
		this.administrador = administrador;
	}
	public String getDni() {
		return dni;
	}
	public Sucursal getSucursal() {
		return sucursal;
	}
	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getAdministrador() {
		return administrador;
	}
	public void setAdministrador(String administrador) {
		this.administrador = administrador;
	}
	public HashMap<Integer, Entrega> getListaEntrega() {
		return listaEntrega;
	}
	public void setListaEntrega(HashMap<Integer, Entrega> listaEntrega) {
		this.listaEntrega = listaEntrega;
	}
	public HashMap<String, Devolucion> getListaDevolucion() {
		return listaDevolucion;
	}
	public void setListaDevolucion(HashMap<String, Devolucion> listaDevolucion) {
		this.listaDevolucion = listaDevolucion;
	}

}
