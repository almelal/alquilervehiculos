package Negocio;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Coche {
	public static final String GAMA_BAJA = "coche gama baja"
			,GAMA_MEDIA="coche gama media"
			,GAMA_ALTA="coche gama alta";
 private String matricula;
 private double km;
 private Categoria categoria;
 private HashMap<Integer, Entrega> listaEntrega = new HashMap<Integer, Entrega>();
 private Sucursal sucursal;
 private String gama;
 
public Coche(String matricula,String gama, double km,Sucursal sucursal,Categoria categoria) {
	this.matricula = matricula;
	this.gama=gama;
	this.km = km;
	this.categoria=categoria;
	this.sucursal = sucursal;
}

public String getGama() {
	return gama;
}

public void setGama(String gama) {
	this.gama = gama;
}

public String getMatricula() {
	return matricula;
}
public void setMatricula(String matricula) {
	this.matricula = matricula;
}
public double getKm() {
	return km;
}
public void setKm(double km) {
	this.km = km;
}
public Categoria getCategoria() {
	return categoria;
}
public void setCategoria(Categoria categoria) {
	this.categoria = categoria;
}
public HashMap<Integer, Entrega> getListaEntrega() {
	return listaEntrega;
}

public void setListaEntrega(HashMap<Integer, Entrega> listaEntrega) {
	this.listaEntrega = listaEntrega;
}

public Sucursal getSucursal() {
	return sucursal;
}
public void setSucursal(Sucursal sucursal) {
	this.sucursal = sucursal;
}

 
}
