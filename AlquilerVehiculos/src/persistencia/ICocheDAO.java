package persistencia;

import excepciones.DAOExcepcion;
import persistencia.dto.CocheDTO;

public interface ICocheDAO {
	public CocheDTO buscarCoche(String matricula)throws DAOExcepcion;
}
