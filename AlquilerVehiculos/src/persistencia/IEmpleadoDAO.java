package persistencia;

import excepciones.DAOExcepcion;
import persistencia.dto.CocheDTO;

public interface IEmpleadoDAO {
	public CocheDTO listarVehiculosDisp(int idSucursal)throws DAOExcepcion;
	public CocheDTO entregarVehiculoReservado (int idVehiculo) throws DAOExcepcion;
/*
 * public ClienteDTO buscarCliente(String dni)throws DAOExcepcion;
 public void crearCliente(ClienteDTO cliente) throws DAOExcepcion;*/
}
