package persistencia;

import excepciones.DAOExcepcion;
import persistencia.dto.EntregaDTO;

public interface IEntregaDAO {
	 public void crearEntrega(EntregaDTO entrega) throws DAOExcepcion;

}
