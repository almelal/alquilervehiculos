package persistencia;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import excepciones.DAOExcepcion;
import persistencia.dto.EmpleadoDTO;

public class EmpleadoDAOImp {

	protected ConnectionManager connManager;

	public EmpleadoDAOImp() throws DAOExcepcion {
		super();
		try{
		connManager= new ConnectionManager("alquilervehiculosBD");
		}
		catch (ClassNotFoundException e){	
			throw new DAOExcepcion(e);
			}
	}
	
	public EmpleadoDTO obtenerEmpleado(String dni) throws DAOExcepcion {
		try{
			
		connManager.connect();
		ResultSet rs=connManager.queryDB("select * from EMPLEADO where DNI = '"+dni+"'");						
		connManager.close();
		if (rs.next())
			return  new EmpleadoDTO(
				rs.getString("DNI").trim(),
				rs.getString("NOMBRE").trim(),
				rs.getBoolean("ADMINISTRADOR"),
				rs.getInt("SUCURSAL"));
		else return null;
		}
		catch (SQLException e){	System.out.println(e);throw new DAOExcepcion(e);}	
	}
	
	public List<EmpleadoDTO> obtenerEmpleados() throws DAOExcepcion {
		try{
			connManager.connect();
			ResultSet rs=connManager.queryDB("select * from EMPLEADO");						
			connManager.close();
	  	  
			List<EmpleadoDTO> listaEmpleadoDTO = new ArrayList<EmpleadoDTO>();
				
			try{				
				while (rs.next()){
					
					/*EMPLEADO(
					 * DNI CHARACTER(10) NOT NULL PRIMARY KEY,
					 * NOMBRE CHARACTER(25) NOT NULL,
					 * ADMINISTRADOR BOOLEAN NOT NULL,
					 * SUCURSAL INTEGER NOT NULL,*/

					EmpleadoDTO empDTO = new EmpleadoDTO(
							rs.getString("DNI").trim(),
							rs.getString("NOMBRE").trim(),
							rs.getBoolean("ADMINISTRADOR"),
							rs.getInt("SUCURSAL")); 
					listaEmpleadoDTO.add(empDTO);
				}
				return listaEmpleadoDTO;
			}
			catch (Exception e){	throw new DAOExcepcion(e);}
		}
		catch (SQLException e){	throw new DAOExcepcion(e);}	
		catch (DAOExcepcion e){		throw e;}

	}
	
}
