package persistencia;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import Util.DateUtil;
import excepciones.DAOExcepcion;
import persistencia.dto.EntregaDTO;

public class EntregaDAOImp implements IEntregaDAO{
	
    protected ConnectionManager connManager;

	public EntregaDAOImp() throws DAOExcepcion {
		super();
		try{
		connManager= new ConnectionManager("alquilervehiculosBD");
		}
		catch (ClassNotFoundException e){	
			throw new DAOExcepcion(e);
			}
	}	
	public List<EntregaDTO> obtenerEntregas() throws DAOExcepcion{
		try{
		connManager.connect();
		ResultSet rs=connManager.queryDB("select * from ENTREGA");						
		connManager.close();
		List<EntregaDTO> listaEntregasDTO = new ArrayList<EntregaDTO>();
					
			while (rs.next()){

				EntregaDTO enDTO = new EntregaDTO(
										rs.getInt("ID")
										, LocalDateTime.of(rs.getDate("FECHA").toLocalDate(),
										      rs.getTime("FECHA").toLocalTime())
										, rs.getString("TIPOSEGURO").trim()
										, rs.getDouble("KMS")
										, rs.getDouble("COMBUSTIBLE")
										, rs.getString("COCHEASIGNADO").trim()
										, rs.getString("EMPLEADOREALIZA").trim());
				listaEntregasDTO.add(enDTO);
			}
			return listaEntregasDTO;
			
		}catch (Exception e){	throw new DAOExcepcion(e);}
	} 
	public void crearEntrega(EntregaDTO entrega) throws DAOExcepcion {
		try{
			connManager.connect();
			connManager.queryDB("insert into ENTREGA values ("
																			+entrega.getId()+",'"
																			+DateUtil.format(entrega.getFecha())+"','"
																			+entrega.getTipoSeguro()+"',"
																			+entrega.getKms()+","																				
																			+entrega.getCombustible()+",'"
																			+entrega.getCocheAsignado()+"','"
																			+entrega.getDniEmp()+"')");	
			
			connManager.queryDB("update COCHE set SUCURSAL = NULL where MATRICULA = '"+entrega.getCocheAsignado()+"'");
			connManager.close();
			
		}catch (SQLException e){System.out.println(e); throw new DAOExcepcion(e);}	
	}
}

//ID INTEGER NOT NULL PRIMARY KEY,
//FECHA TIMESTAMP NOT NULL,
//TIPOSEGURO CHARACTER(10) NOT NULL,
//KMS DOUBLE NOT NULL,
//COMBUSTIBLE DOUBLE NOT NULL,
//COCHEASIGNADO CHARACTER(10) NOT NULL,
//EMPLEADOREALIZA CHARACTER(10) NOT NULL
