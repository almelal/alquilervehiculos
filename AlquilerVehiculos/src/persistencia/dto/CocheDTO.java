package persistencia.dto;

public class CocheDTO {
	
	 private String matricula;
	 private double km;
	 private int idSucursal;
	 private String nombreGama;
	 private String nombreCategoria;
	
	 public CocheDTO(String matricula, double km, int idSucursal,  String nombreCategoria,String nombreGama) {
		super();
		this.matricula = matricula;
		this.km = km;
		this.idSucursal = idSucursal;
		this.nombreGama = nombreGama;
		this.nombreCategoria = nombreCategoria;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public double getKm() {
		return km;
	}

	public void setKm(double km) {
		this.km = km;
	}

	public int getIdSucursal() {
		return idSucursal;
	}

	public void setIdSucursal(int idSucursal) {
		this.idSucursal = idSucursal;
	}

	public String getNombreGama() {
		return nombreGama;
	}

	public void setNombreGama(String nombreCategoriaSuperior) {
		this.nombreGama = nombreCategoriaSuperior;
	}

	public String getNombreCategoria() {
		return nombreCategoria;
	}

	public void setNombreCategoria(String nombreCategoria) {
		this.nombreCategoria = nombreCategoria;
	}
	
	 

	 
}
