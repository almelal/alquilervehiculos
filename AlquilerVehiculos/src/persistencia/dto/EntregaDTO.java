package persistencia.dto;

import java.time.LocalDateTime;


public class EntregaDTO {
	private int id;
	private LocalDateTime fecha;
	private String tipoSeguro;
	private double kms;
	private String cocheAsignado;
	private double combustible;
	private String dniEmp;
	public EntregaDTO(int id, LocalDateTime fecha, String tipoSeguro, double kms, 
			double combustible,String cocheAsignado, String dniEmp) {
		super();
		this.id = id;
		this.fecha = fecha;
		this.tipoSeguro = tipoSeguro;
		this.kms = kms;
		this.cocheAsignado = cocheAsignado;
		this.combustible = combustible;
		this.dniEmp = dniEmp;
	}
	public int getId() {
		return id;
	}
	public void setId(int idSucursal) {
		this.id = idSucursal;
	}
	public LocalDateTime getFecha() {
		return fecha;
	}
	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}
	public String getTipoSeguro() {
		return tipoSeguro;
	}
	public void setTipoSeguro(String tipoSeguro) {
		this.tipoSeguro = tipoSeguro;
	}
	public double getKms() {
		return kms;
	}
	public void setKms(double kms) {
		this.kms = kms;
	}
	public String getCocheAsignado() {
		return cocheAsignado;
	}
	public void setCocheAsignado(String cocheAsignado) {
		this.cocheAsignado = cocheAsignado;
	}
	public double getCombustible() {
		return combustible;
	}
	public void setCombustible(double combustible) {
		this.combustible = combustible;
	}
	public String getDniEmp() {
		return dniEmp;
	}
	public void setDniEmp(String dniEmp) {
		this.dniEmp = dniEmp;
	}
	
		
}


//CREATE MEMORY TABLE PUBLIC.ENTREGA(
//ID INTEGER NOT NULL PRIMARY KEY,
//FECHA TIMESTAMP NOT NULL,
//TIPOSEGURO CHARACTER(10) NOT NULL,
//KMS DOUBLE NOT NULL,
//COMBUSTIBLE DOUBLE NOT NULL,
//COCHEASIGNADO CHARACTER(10) NOT NULL,
//EMPLEADOREALIZA CHARACTER(10) NOT NULL,CONSTRAINT FK_TABLAENTREGA_1 FOREIGN KEY(ID) REFERENCES PUBLIC.RESERVA(ID),CONSTRAINT FK_TABLAENTREGA_3 FOREIGN KEY(EMPLEADOREALIZA) REFERENCES PUBLIC.EMPLEADO(DNI),CONSTRAINT FK_TABLAENTREGA_2 FOREIGN KEY(COCHEASIGNADO) REFERENCES PUBLIC.COCHE(MATRICULA))
