package persistencia.dto;

public class EmpleadoDTO {
	private String dni;
	private String nombre;
	private Boolean admin;
	private int sucursal;
	public EmpleadoDTO(String dni, String nombre, Boolean admin, int sucursal) {
		super();
		this.dni = dni;
		this.nombre = nombre;
		this.admin = admin;
		this.sucursal = sucursal;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Boolean getAdmin() {
		return admin;
	}
	public void setAdmin(Boolean admin) {
		this.admin = admin;
	}
	public int getSucursal() {
		return sucursal;
	}
	public void setSucursal(int sucursal) {
		this.sucursal = sucursal;
	}
	
	


}


/*
 *EMPLEADO(DNI CHARACTER(10) NOT NULL PRIMARY KEY,
 *			NOMBRE CHARACTER(25) NOT NULL,
 *			ADMINISTRADOR BOOLEAN NOT NULL,
 *			SUCURSAL INTEGER NOT NULL,CONSTRAINT FK_EMPLEADO_1 FOREIGN KEY(SUCURSAL) REFERENCES PUBLIC.SUCURSAL(ID))
 * */
 