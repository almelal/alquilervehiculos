package persistencia;

import java.sql.SQLException;
import java.util.List;

import persistencia.dto.CategoriaDTO;
import persistencia.dto.ClienteDTO;
import persistencia.dto.CocheDTO;
import persistencia.dto.EmpleadoDTO;
import persistencia.dto.EntregaDTO;
import persistencia.dto.ReservaDTO;
import persistencia.dto.SucursalDTO;
import excepciones.DAOExcepcion;

public class DAL {
	private static DAL dal;
    // Declaraci�n de los DAO

    private CategoriaDAOImp categoriaDAO;
    private ClienteDAOImp clienteDAO;
    private SucursalDAOImp sucursalDAO;
    private ReservaDAOImp reservaDAO;
    private CocheDAOImp cocheDAO;
    private EmpleadoDAOImp empleadoDAO;
    private EntregaDAOImp entregaDAO;
    
    // constructor privado
    private DAL() throws DAOExcepcion {
    	categoriaDAO = new CategoriaDAOImp();
    	sucursalDAO = new SucursalDAOImp();
    	reservaDAO = new ReservaDAOImp();
    	clienteDAO = new ClienteDAOImp();
    	cocheDAO = new CocheDAOImp();
    	empleadoDAO = new EmpleadoDAOImp();
    	entregaDAO = new EntregaDAOImp();
    }
    
    
    // Patr�n Singleton
    public static DAL dameDAL() throws DAOExcepcion {
    	if (dal == null)
    		dal = new DAL();
    	return dal;
    }

    public List<CategoriaDTO> obtenerCategorias(){
    	try{
    		return categoriaDAO.obtenerCategorias();
    	}catch(DAOExcepcion e){
    		return null;
    	}
   }
    public List<ClienteDTO> obtenerClientes(){
    	try{
    		return clienteDAO.obtenerClientes();
    	}catch(DAOExcepcion e){
    		return null;
    	}
    }
    public List<SucursalDTO> obtenerSucursales(){
    	try{
    		return sucursalDAO.obtenerSucursales();
    	}catch(DAOExcepcion e){
    		return null;
    	}
    }
    public void crearReserva(ReservaDTO reserva){
    	try{
        		reservaDAO.crearReserva(reserva);
        	}catch(DAOExcepcion e){
        		
        	}
    		
    	}
    public ReservaDTO buscarReserva(int reserva){
    	try{
        		return reservaDAO.buscarReserva(reserva);
        	}catch(DAOExcepcion e){
        		return null;
        	}
    		
    	}
    public List<ReservaDTO> obtenerReservas(){
    	try{
    		return reservaDAO.obtenerReservas();
    	}catch(DAOExcepcion e){
    		return null;
    	}
    }
    public List<ReservaDTO> obtenerReservasPorSucursalOrigen(int idSucursal){
    	try{
    		return reservaDAO.obtenerReservasPorSucursalOrigen(idSucursal);
    	}catch(DAOExcepcion e){
    		return null;
    	}
    }
    public void crearCliente(ClienteDTO cliente){
    	try{
    		clienteDAO.crearCliente(cliente);
    	}catch(DAOExcepcion e){
    		
    	}
	}
    public List<CocheDTO> obtenerCoches(){
    	try{
    		return cocheDAO.obtenerCoches();
    	}catch(DAOExcepcion e){
    		return null;
    	}    	
    }
    public List<CocheDTO>obtenerCochesPorSucursal(int idSucursal) throws SQLException{
    	try{
    	return cocheDAO.obtenerCochesPorSucursal(idSucursal);
    }catch(DAOExcepcion e){return null;}
    }
    
    public List<EmpleadoDTO> obtenerEmpleados(){
    	try{
        	return empleadoDAO.obtenerEmpleados();
        }catch(DAOExcepcion e){return null;}
    }
    public EmpleadoDTO obtenerEmpleado(String dni){
    	try{
    		return empleadoDAO.obtenerEmpleado(dni);
    	}catch(DAOExcepcion e){return null;}
    }
    public void crearEntrega(EntregaDTO entrega){
    	try{
    		entregaDAO.crearEntrega(entrega);
    	}catch(DAOExcepcion e){}
    	
    }
    public  List<EntregaDTO> obtenerEntregas() {
    	try{
        	return entregaDAO.obtenerEntregas();
        }catch(DAOExcepcion e){return null;}
    }
}
