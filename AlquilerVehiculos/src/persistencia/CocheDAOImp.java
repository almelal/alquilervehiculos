package persistencia;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import excepciones.DAOExcepcion;
import persistencia.dto.CocheDTO;

public class CocheDAOImp implements ICocheDAO{

	protected ConnectionManager connManager;
	
	public CocheDAOImp() throws DAOExcepcion {
		super();
		try{
		connManager= new ConnectionManager("alquilervehiculosBD");
		}
		catch (ClassNotFoundException e){	
			throw new DAOExcepcion(e);
			}
	}
	
	public CocheDTO buscarCoche(String matricula) throws DAOExcepcion {
		try{
			connManager.connect();
			//cambiar la consulta quitando los coches ya alquilados(poner not exist o minus)
			ResultSet rs=connManager.queryDB("select * from COCHE where MATRICULA= '"+matricula+"'");
			connManager.close();
		
			if (rs.next())
				return new CocheDTO(
							rs.getString("MATRICULA").trim(),
							rs.getDouble("KMSACTUALES"),
							rs.getInt("SUCURSAL"),
							rs.getString("CATEGORIA").trim(),
							rs.getString("NOMBRE").trim());
			else
				return null;	
		}
		catch (SQLException e){	throw new DAOExcepcion(e);}	
	}
	
	public List<CocheDTO> obtenerCoches() throws DAOExcepcion {
		try{
			connManager.connect();
			ResultSet rs=connManager.queryDB("select * from COCHE");						
			connManager.close();
	  	  
			List<CocheDTO> listaCochesDTO = new ArrayList<CocheDTO>();
				
			try{				
				while (rs.next()){

					CocheDTO coDTO = new CocheDTO(
							rs.getString("MATRICULA").trim(),
							rs.getDouble("KMSACTUALES"),
							rs.getInt("SUCURSAL"),
							rs.getString("CATEGORIA").trim(),
							rs.getString("NOMBRE").trim());
					listaCochesDTO.add(coDTO);
				}
				return listaCochesDTO;
			}
			catch (Exception e){	throw new DAOExcepcion(e);}
		}
		catch (SQLException e){	throw new DAOExcepcion(e);}	
		catch (DAOExcepcion e){ throw e;}
		}
	
	public List<CocheDTO> obtenerCochesPorSucursal(int idSucursal) throws DAOExcepcion, SQLException{
		connManager.connect();
		ResultSet rs=connManager.queryDB("select * from COCHE where SUCURSAL ='"+idSucursal+"'");						
		connManager.close();
		List<CocheDTO> listaCochesDTO = new ArrayList<CocheDTO>();
		try{				
			while (rs.next()){

				CocheDTO coDTO = new CocheDTO(
						rs.getString("MATRICULA").trim(),
						rs.getDouble("KMSACTUALES"),
						rs.getInt("SUCURSAL"),
						rs.getString("CATEGORIA").trim(),
						rs.getString("NOMBRE").trim());
				listaCochesDTO.add(coDTO);
			}
			return listaCochesDTO;
		}catch (Exception e){	throw new DAOExcepcion(e);}
	}

}/*fin*/
