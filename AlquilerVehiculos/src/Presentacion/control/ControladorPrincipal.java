package Presentacion.control;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.stage.Stage;
import excepciones.LogicaExcepcion;

public class ControladorPrincipal {
	 private static final String CREAR_CLIENTE = "/Presentacion/vista/crear-cliente.fxml";
	 private static final String LISTAR_RESERVAS_SUCURSAL = "/Presentacion/vista/listar-reservas-sucursal.fxml";
	 private static final String LISTAR_SUCURSALES = "/Presentacion/vista/listar-sucursales.fxml";
	 private static final String CREAR_RESERVA = "/Presentacion/vista/crear-reserva.fxml";
	 private static final String LISTAR_CATEGORIAS = "/Presentacion/vista/listar-categorias.fxml";
	 private static final String LISTAR_VEHICULOS_DISP = "/Presentacion/vista/listar-vehiculos-disp.fxml";
	 private static final String ENTREGA_VR = "/Presentacion/vista/entregaVR.fxml";
	 private Stage primaryStage;
	 @FXML
	 void listarSucursales(ActionEvent event) throws LogicaExcepcion {
		 initCasoDeUso(LISTAR_SUCURSALES,ControladorListarSucursales.class).show();
	 }
	 @FXML
	 void crearCliente(ActionEvent event) throws LogicaExcepcion {
		 initCasoDeUso(CREAR_CLIENTE, ControladorCrearCliente.class).show();
	 }
	 @FXML
	 void crearReserva(ActionEvent event) {
		 initCasoDeUso(CREAR_RESERVA, ControladorCrearReserva.class).show();
	 }
	 @FXML
	 void listarReservasSucursal(ActionEvent event) throws LogicaExcepcion {
		 initCasoDeUso(LISTAR_RESERVAS_SUCURSAL, ControladorListarReservasSucursal.class).show();
	 }
	 @FXML
	 void listarCategorias(ActionEvent event) throws LogicaExcepcion {
		 initCasoDeUso(LISTAR_CATEGORIAS,ControladorListarCategorias.class).show();
	 }
	 @FXML
	 void salir(ActionEvent event) {
		 Platform.exit();
	 }
	 public void setPrimaryStage(Stage primaryStage) {
		 this.primaryStage = primaryStage;
	 }
	 @FXML
	 void listarVehiculosDisp(ActionEvent event) throws LogicaExcepcion {
		 initCasoDeUso(LISTAR_VEHICULOS_DISP, ControladorListarVehiculosDisp.class).show();
	 }
	 @FXML
	  void entregaVR(ActionEvent event) throws LogicaExcepcion {
	   initCasoDeUso(ENTREGA_VR,ControladorEntregarVehiculoReservado.class).show();
	  }
	 private <T extends ControladorCasoDeUso> T initCasoDeUso(String urlVista,Class<T> controlClass) {
	 return ControladorCasoDeUso.initCasoDeUso(urlVista, controlClass,
			 primaryStage, ControladorPrincipal.this); 
	 }
	 
	 
	}