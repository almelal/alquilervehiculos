package Presentacion.control;
import java.net.URL;
import java.util.ResourceBundle;
import excepciones.DAOExcepcion;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import Negocio.AlquilerVehiculos;
import Negocio.Categoria;

public class ControladorListarCategorias extends ControladorCasoDeUso {
	 @FXML private TableView<Categoria> categorias;
	 @FXML private TableColumn<Categoria,String> nombre;
	 @FXML private TableColumn<Categoria, Double> precioKmModKms,precioModKms, precioModIlimitado,precioSeguroTerceros,precioSeguroTRiesgo;
	 @FXML private Button aceptar;
	 
	 @Override
	 public void initialize(URL location, ResourceBundle resources) {
		 stage = new Stage(StageStyle.DECORATED);
		 stage.setTitle("LISTADO DE CATEGORĶAS");
		 aceptar.setOnAction(event -> stage.close());
		 
		 nombre.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue().getNombre()));
		 precioKmModKms.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue().getPrecioKmModKms()));
		 precioModIlimitado.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue().getPrecioModIlimitado()));
		 precioModKms.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue().getPrecioModKms()));
		 precioSeguroTerceros.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue().getPrecioSeguroTerceros()));
		 precioSeguroTRiesgo.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue().getPrecioSeguroTRiesgo()));

		try {
			this.categorias.getItems().addAll(AlquilerVehiculos.newInstancia().getListaCategoria().values());
		} catch (DAOExcepcion e) {
			e.printStackTrace();
		}
	 }
	
}
