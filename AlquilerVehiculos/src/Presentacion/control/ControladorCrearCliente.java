
package Presentacion.control;

import java.net.URL;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Collections;
import java.util.ResourceBundle;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import Negocio.AlquilerVehiculos;
import Negocio.Cliente;
import excepciones.DAOExcepcion;

public class ControladorCrearCliente extends ControladorCasoDeUso {
	
 private boolean dniB;
 @FXML private TextField dni;
 @FXML private TextField nombreApellidos;
 @FXML private TextField direccion;
 @FXML private TextField a�oTC;
 @FXML private TextField mesTC;
 @FXML private TextField codigoPostal;
 @FXML private TextField poblacion;
 @FXML private DatePicker fechaCarnet;
 @FXML private TextField cvc;
 @FXML private TextField tipoTarjeta;
 @FXML private TextField digitosTC;
 @FXML private Button aceptar;
 @FXML private Button cancelar;
 private Cliente nuevoCliente;
 @Override
 public void initialize(URL location, ResourceBundle resources) {
	 stage = new Stage(StageStyle.DECORATED);
	 stage.setTitle("CREAR CLIENTE");
	
	 nombreApellidos.textProperty().addListener(listener ->{
		 							limitarCaracteres(nombreApellidos, 20);
		 							evaluarEstado();
	 								});
	 dni.textProperty().addListener(listener ->{
		 							limitarCaracteres(dni, 10);		 	
		 							validarDni();
		 							evaluarEstado();
		 							});
	 poblacion.textProperty().addListener(listener ->{
		 							limitarCaracteres(poblacion,20);
		 							evaluarEstado();
		 							});
	 direccion.textProperty().addListener(listener ->{
		 							limitarCaracteres(direccion, 20); 
		 							evaluarEstado();
		 							});
	 codigoPostal.textProperty().addListener(listener ->{
		 							limitarCaracteres(codigoPostal,5);
		 							evaluarEstado();
		 							});
	 digitosTC.textProperty().addListener(listener ->{
		 							limitarCaracteres(digitosTC, 16);
		 							evaluarEstado();
		 							});	 
	 mesTC.textProperty().addListener(listener ->{
		 							limitarCaracteres(mesTC, 2);
		 							evaluarEstado();
		 							});
	 a�oTC.textProperty().addListener(listener ->{
		 							limitarCaracteres(a�oTC, 4);
		 							evaluarEstado();
		 							});
	 cvc.textProperty().addListener(listener ->{
		 							limitarCaracteres(cvc, 3);
		 							evaluarEstado();
		 							});
	 tipoTarjeta.textProperty().addListener(listener ->{
		 							limitarCaracteres(tipoTarjeta, 10);
		 							evaluarEstado();
		 							});
	 fechaCarnet.setOnAction(event ->evaluarEstado());// POR HACER -> Evaluar que la fecha sea anterior a la de hoy?
	 
	 
	 cancelar.setOnAction(event -> stage.close());
	 aceptar.setOnAction(event -> {
	 nuevoCliente = new Cliente(dni.getText(), 
			 					nombreApellidos.getText(),
			 					direccion.getText(),
			 					poblacion.getText(), 
			 					codigoPostal.getText(),
			 					LocalDateTime.of(fechaCarnet.getValue(),  LocalTime.of(0, 0)), 
			 					digitosTC.getText(),
			 					Integer.parseInt(mesTC.getText()),
			 					Integer.parseInt(a�oTC.getText()),
			 					Integer.parseInt(cvc.getText()), 
			 					tipoTarjeta.getText());
	
	 if (nuevoCliente != null) {
	 //Invocamos el servicio encargado de Crear un nuevo cliente
	try {
		AlquilerVehiculos.newInstancia().crearCliente(nuevoCliente);
	} catch (Exception e) {
		e.printStackTrace();
	}
	 //LOG.log(Level.INFO, "Se ha creado un nuevo Cliente: " +nuevoCliente);
	 } else {
	 //LOG.log(Level.INFO, "No se ha podido crear un nuevo cliente.");
	 }
	 stage.close();
	 });
	 //
	 aceptar.setDisable(true);
 }
 private void evaluarEstado(){
	 //Comprobamos que los campos est�n rellenos
	 try{
	 boolean 
			 nombreB = nombreApellidos.getLength() > 0,
			 direccionB = direccion.getLength() > 0,
			 poblacionB = poblacion.getLength() > 0,
			 codigoPostalB = codigoPostal.getLength() > 0,
			 digitosB = digitosTC.getLength() > 0,
			 a�oB = Integer.parseInt(a�oTC.getText()) > 0 && Integer.parseInt(a�oTC.getText()) < 10000,
			 mesB = Integer.parseInt(mesTC.getText()) > 0 && Integer.parseInt(mesTC.getText()) < 13,
			 cvcB = Integer.parseInt(cvc.getText()) > 0 && Integer.parseInt(cvc.getText()) < 1000,
			 tipoTarjetaB = tipoTarjeta.getLength() > 0,
			 fechaB = fechaCarnet.getValue() != null;
			 
			 
	 if(dniB && nombreB && direccionB && poblacionB && codigoPostalB && digitosB && a�oB && mesB && cvcB && tipoTarjetaB && fechaB){
		aceptar.setDisable(false);
	}else
		aceptar.setDisable(true);
	 }catch(Exception e){ aceptar.setDisable(true);} 
 }
 
 private void limitarCaracteres(TextField tf, int max){
	 if(tf.getLength()>max) tf.setText(tf.getText().substring(0,max));	 
 }
 private void validarDni(){
	 ObservableList<String> styleClass = dni.getStyleClass();
	 	 try {
		if(AlquilerVehiculos.newInstancia().getListaCliente().get(dni.getText())!=null){
				aceptar.setDisable(true);//el cliente ya est� en la BD	
				dniB = false;
				if(!styleClass.contains("error")) 
					styleClass.add("error");
		 }else{
		 dniB = true;
		 styleClass.removeAll(Collections.singleton("error"));
		 }
	} catch (DAOExcepcion e) {
		e.printStackTrace();
	}
 }
}