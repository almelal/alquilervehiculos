package Presentacion.control;

import java.net.URL;
import java.sql.SQLException;
import java.util.Collection;
import java.util.ResourceBundle;

import Negocio.AlquilerVehiculos;
import Negocio.Sucursal;
import excepciones.DAOExcepcion;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import persistencia.dto.CocheDTO;

public class ControladorListarVehiculosDisp extends ControladorCasoDeUso{
	
	@FXML    private TableColumn<CocheDTO,String > matric;
    @FXML    private TableColumn<CocheDTO,String > nom;
    @FXML    private TableColumn<CocheDTO,String > cat;
    @FXML    private Button cerrar;
    @FXML    private ComboBox<String> listaSucursales;
    @FXML    private TableColumn<CocheDTO, Double> kmsact;
    @FXML    private TableView<CocheDTO> vehiculos;
	//lista observable de las sucursales
	 private ObservableList<String> listarSuc(Collection<Sucursal> l){
		 ObservableList<String> lista = FXCollections.observableArrayList();
				for(Sucursal ca:l){
					lista.add(ca.getDireccion());
				}
		return lista;
	}
	 //elegir sucursal
	 private void elegirSucursal() throws SQLException{
		 try {
			 ObservableList<CocheDTO> lista = FXCollections.observableArrayList();
				for ( CocheDTO re : AlquilerVehiculos.newInstancia().getCochesSucursal(listaSucursales.getItems().indexOf(listaSucursales.getValue())+1)){
						//ReservasPorSucursal(listaSucursales.getItems().indexOf(listaSucursales.getValue())+1)){
					lista.add(re);
				}
				if (lista.size()==0) error();

				this.vehiculos.getItems().clear();
				this.vehiculos.getItems().addAll(lista);
			
			} catch (DAOExcepcion e) {
				e.printStackTrace();
			}
		 nom.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue().getNombreGama()));
		 matric.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue().getMatricula().toString()));
		 kmsact.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue().getKm()));
		 cat.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue().getNombreCategoria()));
	 }
	 @Override
	 public void initialize(URL location, ResourceBundle resources) {
		 stage = new Stage(StageStyle.DECORATED);
		 stage.setTitle("LISTADO DE RESERVAS POR SUCURSAL");
		 cerrar.setOnAction(event -> stage.close());
		 try {
			listaSucursales.setItems(listarSuc(AlquilerVehiculos.newInstancia().getListaSucursal().values()));
			
			
		 } catch (DAOExcepcion e) {
			e.printStackTrace();
		}
		 
		 listaSucursales.setOnAction(a ->{ 
			 try {
				elegirSucursal();
			} catch (Exception e) {
				e.printStackTrace();
			}});	
		
	 }
	 private void error(){
			Alert alert = new Alert(AlertType.ERROR);
			 alert.setTitle("Error");
			 alert.setHeaderText("No hay coches");
			 alert.setContentText("No hay coches en la sucursal");
			 alert.showAndWait();
		}
}
