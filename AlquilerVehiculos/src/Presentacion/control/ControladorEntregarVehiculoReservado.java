package Presentacion.control;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.text.html.HTMLDocument.Iterator;

import application.AlquilerVehiculosApp;
import Negocio.AlquilerVehiculos;
import Negocio.Categoria;
import Negocio.Coche;
import Negocio.Empleado;
import Negocio.Entrega;
import Negocio.Reserva;
import Negocio.Sucursal;
import excepciones.DAOExcepcion;
import excepciones.LogicaExcepcion;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import persistencia.dto.CocheDTO;
import persistencia.dto.EntregaDTO;
import persistencia.dto.ReservaDTO;

public class ControladorEntregarVehiculoReservado extends ControladorCasoDeUso {
	 @FXML	 private TableView<Reserva> reservas;
	 @FXML	 private TableView<Coche> coches;
	 @FXML	 private TableColumn<Coche, String> cocheC;
	 @FXML	 private TableColumn<Reserva, Integer> mod,id;
	 @FXML	 private TableColumn<Reserva, String> fecharR,fecharD,cat,dniC,nombre;
	 @FXML	 private Button efectuar;
	 @FXML   private TextField kilometros,combustible;
	 @FXML   private ComboBox<String> tipoSeguro,empleado;
	 
	 Stage primaryStage;
	 
	 @Override
	 public void initialize(URL location, ResourceBundle resources) {
		 stage = new Stage(StageStyle.DECORATED);
		 stage.setTitle("ENTREGA DE RESERVAS");
		
		 reservas.setOnMousePressed(event->{
			 try {
				mostrarDatos();
			} catch (Exception e) {
				e.printStackTrace();
			}
		 });
		 efectuar.setOnAction(rvrnt->{realizarEntrega();});
		 id.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue().getIdentificador()));
		 fecharR.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue().getFechaRecogida().toString()));
		 fecharD.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue().getFechaDevolucion().toString()));
		 cat.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue().getCategoriaAsociada().getNombre().toString()));
		 mod.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue().getModalidadAlquiler()));
		 dniC.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue().getCliente().getIdentificador()));
		 nombre.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue().getCliente().getNombreyApellidos()));
		 cargarReservas();
		 mostrarTipoSeguro();
		}

	private void realizarEntrega() {
		int idReserva = reservas.getSelectionModel().getSelectedItem().getIdentificador();
	
		try {
				Entrega entrega = new Entrega(
						idReserva //??????? Fallo en la BD
						, LocalDateTime.now()
						, tipoSeguro.getValue()
						, Double.parseDouble(kilometros.getText())//coches.getSelectionModel().getSelectedItem().getKm()
						, Double.parseDouble(combustible.getText())
						, coches.getSelectionModel().getSelectedItem()
						, AlquilerVehiculos.newInstancia().getEmpleado(empleado.getValue()));
				 AlquilerVehiculos.newInstancia().crearEntrega(entrega);
				 stage.close();
			} catch (NumberFormatException | DAOExcepcion e) {
				System.out.println("ERROR!");
				e.printStackTrace();
			}
	
		
	}

	private void mostrarDatos() throws SQLException {
		//System.out.println("estamos en mostrar datos");	
		
		mostrarCoches();
		listarEmpleados();
		
		
	}
    private ObservableList<Coche> listarCoches(Collection<Coche> l){
		 ObservableList<Coche> lista = FXCollections.observableArrayList();
		 Categoria cat =reservas.getSelectionModel().getSelectedItem().getCategoriaAsociada();
		 boolean hayCoches = false;
		 while(!hayCoches){		
			 for(Coche co:l){
					if(cat.getNombre() == co.getCategoria().getNombre()){
						lista.add(co);
						hayCoches = true;
					}
				}
			 cat = cat.getSuperior();
			 if (cat == null && !hayCoches) return null;
		 	}
		return lista;
	}
    
    private void listarEmpleados(){
    	ObservableList<String> empleados = FXCollections.observableArrayList();
    	try {
    		empleado.getItems().clear();
			for(Empleado em : AlquilerVehiculos.newInstancia().getListaSucursal().get(reservas.getSelectionModel().getSelectedItem().getLugarR().getId()).getListaEmpleado().values()){
			
				empleado.getItems().add(em.getDni());
			}
		} catch (DAOExcepcion e) {
			e.printStackTrace();
		}
    }
    private void mostrarCoches(){
		ObservableList<Coche> listaCoches = listarCoches(reservas.getSelectionModel().getSelectedItem().getLugarR().getListaCoche().values());
		if(listaCoches == null || listaCoches.size()<1){
			efectuar.setDisable(true);
			errorCampos();
		}
		else efectuar.setDisable(false);
		coches.setItems(listaCoches);
		cocheC.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue().getMatricula()));
    }
    private void mostrarTipoSeguro(){
    	tipoSeguro.getItems().add("TodoRiesgo");
    	tipoSeguro.getItems().add("Terceros");
    	
    }
    private void cargarReservas(){
    	try {
			List<Reserva> listaReservas = new ArrayList<Reserva>();
			listaReservas.addAll(AlquilerVehiculos.newInstancia().getListaReserva().values());
			for (Reserva re : listaReservas){
				if(re.getEntrega() == null) this.reservas.getItems().add(re);
			}
			 
		} catch (DAOExcepcion e) {
			e.printStackTrace();
		}
    }
    private void errorCampos(){
		Alert alert = new Alert(AlertType.ERROR);
		 alert.setTitle("Error");
		 alert.setHeaderText("No hay coches");
		 alert.setContentText("No hay coches de la gama seleccionada o superior en la sucursal");
		 alert.showAndWait();
	}
}

	