package Presentacion.control;

import java.net.URL;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Collection;
import java.util.Collections;
import java.util.ResourceBundle;
import excepciones.DAOExcepcion;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import Negocio.AlquilerVehiculos;
import Negocio.Categoria;
import Negocio.Sucursal;

public class ControladorCrearReserva extends ControladorCasoDeUso {
	
	@FXML  private TextField identificador;
	@FXML  private ComboBox<String> lugarDev;
    @FXML  private ComboBox<String> lugarRec;
    @FXML  private ComboBox<String> modAlquiler;
    @FXML  private ComboBox<String> catAsociada;
    @FXML  private DatePicker fechaR;
    @FXML  private DatePicker fechaD;
    @FXML  private Button aceptar;
    @FXML  private Button cancelar;
   
    
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
   	 stage = new Stage(StageStyle.DECORATED);
   	 stage.setTitle("CREAR RESERVA");
   	 
	try {
   	 ObservableList<String> listaCat = listarCat(AlquilerVehiculos.newInstancia().getListaCategoria().values());
	 catAsociada.setItems(listaCat);
	 
	 ObservableList<String> listaSuc = listarSuc(AlquilerVehiculos.newInstancia().getListaSucursal().values());
	 lugarDev.setItems(listaSuc);
	 lugarRec.setItems(listaSuc);
	} catch (DAOExcepcion e) {e.printStackTrace();}	
	
	 ObservableList<String> listaMod = listarMod();
	 modAlquiler.setItems(listaMod);
	 /*Listeners para el estado */
	 identificador.textProperty().addListener(listener -> {
		 									limitarCaracteres(identificador, 10);
		 									comprobarDni();
	 										});
	 
   	 cancelar.setOnAction(event -> stage.close());
   	 aceptar.setOnAction(event ->  aceptarReserva());
   	 
	}

    private ObservableList<String> listarCat(Collection<Categoria> l){
		 ObservableList<String> lista = FXCollections.observableArrayList();
				for(Categoria ca:l){
					lista.add(ca.getNombre());
				}
		return lista;
	}
    
    private ObservableList<String> listarSuc(Collection<Sucursal> l){
		 ObservableList<String> lista = FXCollections.observableArrayList();
				for(Sucursal ca:l){
					lista.add(ca.getDireccion());
				}
		return lista;
	}
    private ObservableList<String> listarMod(){
		 ObservableList<String> lista = FXCollections.observableArrayList();
		 lista.add("Km ilimitados");
		 lista.add("Por Km");
		return lista;
	}
    
	private boolean evaluarEstado(){
		try{
			 boolean idB = identificador.getLength() > 0,
					 fechaRB = fechaR.getValue()!=null,
					 fechaDB = fechaD.getValue()!=null,
					 lugarDevB = lugarDev.getValue()!=null,
					 modAlquilerB = modAlquiler.getValue()!=null,
					 catAsociadaB = catAsociada.getValue()!=null;
			 if(idB && fechaRB && fechaDB && lugarDevB && modAlquilerB && catAsociadaB)
				return true;			 
			}catch(Exception e){} 
		return false;
	}
	
	private void aceptarReserva(){
		try {
   		 	if(AlquilerVehiculos.newInstancia().getListaCliente().get(identificador.getText())==null){
   		 		ControladorCrearCliente crearCl = initCasoDeUso("/Presentacion/vista/crear-cliente.fxml", ControladorCrearCliente.class, stage,controladorPrincipal);
   		 		crearCl.show();
   		 		crearCl.getStage().setOnHiding(value ->comprobarDni());//Al cerrar la ventana de crear cliente recomprobamos si existe
   		 	}else{   
   		 		if(evaluarEstado()){
   		 		AlquilerVehiculos.newInstancia().crearReserva(LocalDateTime.of(fechaR.getValue(),LocalTime.of(0, 0)),
										LocalDateTime.of(fechaD.getValue(),LocalTime.of(0, 0)),
										AlquilerVehiculos.newInstancia().encontrarS((String)lugarRec.getValue()),
										AlquilerVehiculos.newInstancia().encontrarS((String)lugarDev.getValue()),
										identificador.getText(),
										(String)catAsociada.getValue(),
										modAlquiler.getItems().indexOf(modAlquiler.getValue())+1);
				stage.close();}
   		 		else{ 
   		 			errorCampos();
   		 			}
   		 		}
   		 	} catch (Exception e) {e.printStackTrace();}
		
	}
	private void limitarCaracteres(TextField tf, int max){
		 if(tf.getLength()>max) tf.setText(tf.getText().substring(0,max));	 
	 }
	private void comprobarDni(){
		 ObservableList<String> styleClass = identificador.getStyleClass();
		 	 try {
			if(AlquilerVehiculos.newInstancia().getListaCliente().get(identificador.getText())==null){			
				if(!styleClass.contains("error")) 
						styleClass.add("error");
				aceptar.setText("Crear Cliente");
				//incluir texto? "El usuario no se encuentra en la BD"
			 }else{
				 styleClass.removeAll(Collections.singleton("error"));
				 aceptar.setText("Aceptar");
			 }
		} catch (DAOExcepcion e) {
			e.printStackTrace();
		}
	 }
	private void errorCampos(){
		Alert alert = new Alert(AlertType.ERROR);
		 alert.setTitle("Error");
		 alert.setHeaderText("Error intentando crear la Reserva:");
		// � null si no queremos cabecera
		 alert.setContentText("Aseg�rate de que has rellenado todos los campos correctamente");
		 alert.showAndWait();
	}
}
